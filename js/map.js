window.onload = function() {
// Create map instance
var chart = am4core.createFromConfig( {
    "series": [{
    "type": "MapPolygonSeries",
    "dataFields": {
      "value": "litres",
      "category": "country"
    }
  }]
} ,"chartdiv", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldLow;

// Set projection //MILLER
chart.projection = new am4maps.projections.Miller();

// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
//Color del mapa
polygonTemplate.fill = am4core.color("#3A7CA5");

// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#6196b7");

//Controles
chart.zoomControl = new am4maps.ZoomControl();
chart.zoomControl.slider.height = 100;

//Permite mover el mapa en pantallas tactiles arrastrando el dedo (si clickas primero dentro del mapa)
chart.panEventsEnabled = true;

//Mini-mapa
chart.smallMap = new am4maps.SmallMap();
chart.smallMap.series.push(polygonSeries);
chart.smallMap.align = "right";
chart.smallMap.valign = "top";
//Bordes Rectangulo (1-Color, 2-Anchura)
chart.smallMap.rectangle.stroke = am4core.color("#367B25");
chart.smallMap.rectangle.strokeWidth = 2;
//Bordes Background
chart.smallMap.background.stroke = am4core.color("#7B3625")
chart.smallMap.background.strokeOpacity = 1;
chart.smallMap.background.fillOpacity = 0.9;

//Colorear España
polygonSeries.data = [{
    "id": "ES",
    "name": "Spain",
    "fill": am4core.color("#173F5F")
}];

//Centramos el mapa (0 mas o menos España) al modificar se modifican los limites del mapa para readaptarse
chart.deltaLongitude = 0;

// Remove Antarctica
polygonSeries.exclude = ["AQ"];

//Boton goHome (centrar zoom posicion inicial)
var home = chart.chartContainer.createChild(am4core.Button);
home.label.text = "Reset Zoom";
home.align = "center";
home.events.on("hit", function(ev) {
  chart.goHome();
});

// Bind "fill" property to "fill" key in data
polygonTemplate.propertyFields.fill = "fill";

// Create image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());

// Create a circle image in image series template so it gets replicated to all new images
var imageSeriesTemplate = imageSeries.mapImages.template;
var circle = imageSeriesTemplate.createChild(am4core.Circle);
circle.radius = 4;
//Color del PIN
circle.fill = am4core.color("#F18701");
circle.stroke = am4core.color("#F5BB00");
circle.strokeWidth = 2;
circle.nonScaling = true;
circle.tooltipText = "{title}";
circle.readerDescription  = "{info}";

// Set property fields
imageSeriesTemplate.propertyFields.latitude = "latitude";
imageSeriesTemplate.propertyFields.longitude = "longitude";

// Add data for the cities
imageSeries.data = [{
    "latitude": -12.0713127,
    "longitude": -77.0014671,
    "title": "San Luis, Perú",
    "nombre" : "Blumaq Perú S.A.C.",
    "info" : "T: +51 1 5003890 <br> Email: blumaqperu@blumaq.com"
}, {
    "latitude": -33.45694,
    "longitude": -70.64827,
    "title": "Santiago, Chile",
    "nombre" : "Blumaq Chile",
    "info" : "T: +56 27 391 257 <br> F: +56 27 391 527 <br> Email: blumaqchile@blumaq.com"
}, {
    "latitude": 25.8651,
    "longitude": -80.3245,
    "title": "Hialeah Gardens, Florida",
    "nombre" : "Blumaq Corp",
    "info" : "Toll Free Sales: +1 844 314 9016 <br> T: +1 305 558 4051 <br> F: +1 305 558 4262 <br> Email: blumaqmiami@blumaq.com"
}, {
    "latitude": 39.803414557216,
    "longitude": -0.22084349171322515,
    "title": "La Vall d'Uxó, Castellón, España",
    "nombre" : "Blumaq Central",
    "info" : "T +34 964 697 030 <br> T +34 964 697 040 <br> Email: blumaq@blumaq.es"
}, {
    "latitude": 35.0366427,
    "longitude": -89.9189314,
    "title": "Menphis, Tennessee",
    "nombre" : "Blumaq Corp",
    "info" : "Toll Free Sales: +1 844 314 9016 <br> T: +1 901 347 3217 <br> F: +1 812 615 0194 <br> Email: blumaqmemphis@blumaq.com"
}, {
    "latitude": -26.256617,
    "longitude": 28.182292,
    "title": "Germiston, Sudáfrica",
    "nombre" : "Blumaq South Africa (Pty) Ltd.",
    "info" : "T: +27 0119665092 <br> Email: blumaqsouthafrica@blumaq.com"
}, {
    "latitude": -12.80243,
    "longitude": 28.21323,
    "title": "Copperbelt, Zambia",
    "nombre" : "Blumaq Zambia Limited",
    "info" : "T: +260966469200 <br> Email: zambia@blumaq.com"
}, {
    "latitude": 29.87819,
    "longitude": 121.54945,
    "title": "Ningbo, China",
    "nombre" : "Blumaq China",
    "info" : "T: +86 574 2795 1987 <br> F: +86 574 2795 1986 <br> Email: blumaqchina@blumaq.com"
}, {
    "latitude": 59.93863,
    "longitude": 30.31413,
    "title": "St. Petersburg, Rusia",
    "nombre" : "Blumaq Rusia",
    "info" : "T: +7(812)612-25-75 <br> Email: blumaqrussia@blumaq.com"
},{
    "latitude": 55.760039,
    "longitude": 37.852694,
    "title": "Moscow, Rusia",
    "nombre" : "Blumaq Co. Ltd",
    "info" : "T: +7(495)664-62-14 <br> Email: blumaqmoscow@blumaq.com"
}, {
    "latitude": 38.2503903,
    "longitude": 27.134193,
    "title": "Menderes, Turquia",
    "nombre" : "Blumaq",
    "info" : "T: +90 232 502 32 35 <br> F: +90 232 502 32 36 <br> Email: blumaqizmir@blumaq.com"
}, {
    "latitude": 39.91987,
    "longitude": 32.85427,
    "title": "Ankara, Turquia",
    "nombre" : "Blumaq Makine Sanayi ve Ticaret",
    "info" : "T: +90 3125028381 <br> F: +90 3125028372 <br> Email: blumaqankara@blumaq.com"
}, {
    "latitude": 40.93567,
    "longitude": 29.15507,
    "title": "Estambul, Turquia",
    "nombre" : "Blumaq Endüstriyel Makina Ürünleri San ve Tic. Ltd Sti.",
    "info" : "T: +90 216 489 87 40 <br> F: +90 216 366 85 67 <br> Email: blumaqturkey@blumaq.com"
}, {
    "latitude": 46.0823755,
    "longitude": 23.5909726,
    "title": "Alba Iulia, Rumanía",
    "nombre" : "Blumaq Romania",
    "info" : "T: +40 358401 110 <br> F: +40 358401 110 <br> Email: blumaqromania@blumaq.com"
}, {
    "latitude": 44.5630515,
    "longitude": 11.271693,
    "title": "Calderara di Reno, Italia",
    "nombre" : "Blumaq Italia S.R.L.",
    "info" : "T: +39051733118 <br> F: +39051731953 <br> Email: blumaqitalia@blumaq.com"
}, {
    "latitude": 48.7664036,
    "longitude": 2.1211908,
    "title": "Buc, Francia",
    "nombre" : "Blumaq Francia SARL",
    "info" : "T: +33(0) 130 830 952 <br> F: +33(0) 139 566 030 <br> Email: blumaqfrancia@blumaq.com"
}, {
    "latitude": 43.521805,
    "longitude": 5.452291,
    "title": "Aix-en-Provence, Francia",
    "nombre" : "Blumaq Francia",
    "info" : "T: +33(0) 442 500 515 <br> F: +33(0) 442 509 485 <br> Email: blumaqpaca@blumaq.com"
}, {
    "latitude": 39.3877151,
    "longitude": -9.0553983,
    "title": "Casalinho, Portugal",
    "nombre" : "Blumaq Peças para Máquinas Industriais Lda.",
    "info": "T: +351 244 802 342 <br> F: +351 244 802 334 <br> Email: blumaqportugal@blumaq.com"
}];

// Make globe rotatable
/* Not globe anymore
chart.seriesContainer.draggable = false;
chart.seriesContainer.resizable = false;

var originalDeltaLongitude = 0;

chart.seriesContainer.events.on("down", function(ev) {
  originalDeltaLongitude = chart.deltaLongitude;
});

chart.seriesContainer.events.on("track", function(ev) {
  if (ev.target.isDown) {
    var pointer = ev.target.interactions.downPointers.getIndex(0);
    var startPoint = pointer.startPoint;
    var point = pointer.point;
    var shift = point.x - startPoint.x;
    chart.deltaLongitude = originalDeltaLongitude + shift / 2;
  }
})
*/
//Evento click: muestra info
circle.events.on("hit", mostrarInfo);

//Manejador mostrar info
// @param event: Objeto que activa el evento.
function mostrarInfo(event)
{
    document.getElementById("info").innerHTML = '<p><b>' + event.target.dataItem._dataContext.nombre + "  -  " + event.target.dataItem._dataContext.title + '</b></p><p>' + event.target.dataItem._dataContext.info  + '</p>';
}

}
