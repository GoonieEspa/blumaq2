﻿$(document).ready(function(){

    $('.carousel').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    
    $('.slider-nav').not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.carousel',
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true
    });



/*         prevArrow:"<img class='a-left control-c slick-arrow prev home-prev slick-prev'>",
        nextArrow:"<img class='a-right control-c slick-arrow next home-next slick-next'>",
         */
    $('.home-slider').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        infinite: true,
        arrows: true,
        prevArrow: "<button class='slick-prev slick-arrow' aria-label='Previous' type='button' style='display: block;'>Previous</button>",
        nextArrow: "<button class='slick-next slick-arrow' aria-label='Next' type='button' style='display: block;'>Next</button>",
        fade: true,
        speed: 1000,
        slidesToShow: 1
    });

    $('.about-slider').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        infinite: true,
        fade: true,
        speed: 1000,
        slidesToShow: 1
    });

    $('.html5-videos').lightGallery({
            addClass:'localVideo',
            auto: false,
	    enableSwipe: false
    }); 

      var $slide = document.getElementById('carousel').getElementsByClassName('slick-slide');
      var slideIndex = $slide[$slide.length-1].getAttribute("data-slick-index");


    //FUNCION CAMBIO DE DISPLAY EN GALERIA (SI VA)
    var videos = document.getElementById('videos');
    var photos = document.getElementById('photos');
    $('#multimedia-videos-trigger').on('click', function() {
        videos.style = "display: flex;";
        photos.style = "display: none;";
    });

    /* $('#multimedia-fotos-trigger').on('click', function() {
        photos.style = "display: flex;";
        videos.style = "display: none;";
    }); */ 

    //FUNCION REEMPLAZA IMAGENES CAROUSEL
    



    $('.producto').on('click', controlFamilias);
    function controlFamilias(e)
    {
        var id = event.target.id;
        var ruta = "images/" + id + "/";
        //var arrayImagenes = document.getElementById('carousel').getElementsByTagName('img');
        
        //Borra todo el contenido menos la imagen de index 0 (la primera) (funciona)
        for(var i = 0; slideIndex > 0; i++)
        {
            $('.carousel').slick('slickRemove',slideIndex - 1);
            $('.slider-nav').slick('slickRemove',slideIndex - 1);
            if (slideIndex !== 0)
            {
                slideIndex--;
            }
        }
        $('.carousel').slick('slickRemove',slideIndex = 0);
        $('.slider-nav').slick('slickRemove',slideIndex = 0);

        //Añade imagenes al carousel y al nav (funciona)
        var limite = event.target.getAttribute('data-numero');
        for(var i = 0; i <= limite; i++)
        {
            slideIndex++;
            rutaCompleta = ruta + i + ".jpg";
            /* $('.carousel').slick('slickAdd', '<div><img src=\"' + rutaCompleta + '\"></div>'); */
            /* VERSION AÑADE IMAGEN EN BACKGROUND PARA CENTRAR SIN PROBLEMA */
            $('.carousel').slick('slickAdd', '<div><img style=\" background-image: url(' + rutaCompleta + '); background-size: cover; background-repeat: no-repeat; background-position: center center;\"></div>');
            $('.slider-nav').slick('slickAdd','<div><img src=\"' + rutaCompleta + '\"></div>');
        }

        var familia = document.getElementById('nombre-familia');
        var carousel = document.getElementById('carousel');
        carousel.style = "background-image: none";
        switch(Number(id))
        {
            case 1:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Cab Components</span><a href='multimedia/1.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 2:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Chassis</span><a href='multimedia/2.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 3:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Filters</span><a href='multimedia/3.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 4:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Hydraulics</span>";
            break;

            case 5:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>High Wear Items</span><a href='multimedia/5.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 6:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Gaskets and Seals</span><a href='multimedia/6.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 7:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Engine</span><a href='multimedia/7.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 8:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Maintenance Products</span><a href='multimedia/8.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 9:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Electrical Parts</span><a href='multimedia/9.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 10:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Cooling System</span>";
            break;

            case 11:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Hardware</span><a href='multimedia/11.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 12:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Bearings</span><a href='multimedia/12.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 13:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Undercarriage</span><a href='multimedia/13.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 14:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Powertrain</span>";
            break;

            case 15:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Brakes</span><a href='multimedia/15.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 16:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Alternative spare parts Caterpillar</span><a href='multimedia/16.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 17:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Spare parts for Volvo</span><a href='multimedia/17.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 18:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Spare parts for Case</span><a href='multimedia/18.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 19:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Spare parts for Komatsu</span><a href='multimedia/19.pdf' target='_blank'><img class='pdf-icon' src='multimedia/pdf.jpg'></a>";
            break;

            case 20:
                familia.innerHTML = "<span class='section-title__subtitle' style='text-transform: none;'>Spare parts for Cummins</span>";
            break;
        }
    }

    

    
});